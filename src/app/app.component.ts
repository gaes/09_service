import { Component,OnInit } from '@angular/core';
import { Country } from './classes/country';
import { CountryService } from './services/country.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  
  coutries: Country[];
  selectedCountry: Country;
 
  constructor(private coutryService: CountryService) {

  }

  getCountries(): void {
    //this.coutryService.getCountries().then(coutries => this.coutries = coutries);
    this.coutryService.getCountries().then(coutries => {    
       this.coutries = coutries;
    }).catch(e => {
      console.log(e);
    });

    
  }

  ngOnInit(): void {
    this.getCountries();
  }

  onSelect(country: Country): void {
    this.selectedCountry = country;
  }



}