import { Injectable } from '@angular/core';
import { Country } from '../classes/country';

@Injectable()
export class CountryService {
    
   countries: Country[];

   
    getCountries(): Promise<Country[]> {        
        this.countries = [
            { id: 1, name: 'Italy' },
            { id: 2, name: 'Spain' },
            { id: 3, name: 'France' },
            { id: 4, name: 'Denmark' },
            { id: 5, name: 'Belgio' },
            { id: 6, name: 'England' },
            { id: 7, name: 'Greece' },
            { id: 8, name: 'Usa' },
            { id: 9, name: 'Canada' },
            { id: 10, name: 'Mexico' }
        ];

        console.log(this.countries);
        return Promise.resolve(this.countries);
    }

}
