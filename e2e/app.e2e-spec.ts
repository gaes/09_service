import { ServicePage } from './app.po';

describe('service App', () => {
  let page: ServicePage;

  beforeEach(() => {
    page = new ServicePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
